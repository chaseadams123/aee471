package com.school.AEE471;

public class Homework3Part1 {

	public static void main(String[] args) throws InterruptedException {

		final CSolver cInterface = new CSolver();

		// define the mesh length and create the mesh array
		final int M = 256;
		final double meshLength = 1.0;

		final double[] mesh = new double[M + 1];

		for (double x = 0; x <= M; x++) {
			mesh[(int) x] = meshLength * (x / M);
		}

		final double[] actualVal = new double[mesh.length];

		{

			for (int x = 0; x < mesh.length; x++) {
				actualVal[x] = (1.0 / 4.0) * ((5.0 * mesh[x]) - 4.0) * Math.pow(mesh[x], 3.0) * Math.sin(2.0 * Math.PI * mesh[x]);
			}

		}

		final int size = mesh.length;

		final double h = (meshLength / size);

		{

			System.out.println("mesh length: " + mesh.length);

			final long timeStart = System.nanoTime();

			final CPlot plot = new CPlot("Point Jacobi - Phi vs. X");

			for (int iterations = 100; iterations < 501; iterations += 100) {

				// solve for the solutions to the derivatives at each point
				final double[] guess = cInterface.solveSecondDerivativeCentralDifference(
						getGuessValue(mesh), h);

				System.out.println("last vals java - " + guess[guess.length - 1] + ", " + guess[guess.length - 2] + ", " + guess[guess.length - 3]);

				final double[] pj = cInterface.findOneDimensionalPointJacobi(iterations, guess,
						getSecondDerValue(mesh), h);

				plot.addLinePlot(iterations + " Iterations", mesh, pj);

			}

			// print the time it took to run this section
			System.out.println("time took: " + (System.nanoTime() - timeStart) + " ns\n");

			plot.addLinePlot("Initial Guess", mesh, getGuessValue(mesh));

			plot.addLinePlot("True Value", mesh, actualVal);

		}

		{

			System.out.println("mesh length: " + mesh.length);

			final long timeStart = System.nanoTime();

			final CPlot plot = new CPlot("Gauss Seidel - Phi vs. X");

			for (int iterations = 100; iterations < 501; iterations += 100) {

				// solve for the solutions to the derivatives at each point
				final double[] guess = cInterface.solveSecondDerivativeCentralDifference(
						getGuessValue(mesh), h);

				System.out.println("last vals java - " + guess[guess.length - 1] + ", " + guess[guess.length - 2] + ", " + guess[guess.length - 3]);

				final double[] gs = cInterface.findOneDimensionalGaussSeidel(iterations, guess,
						getSecondDerValue(mesh), h, 0.0, 0.0);

				plot.addLinePlot(iterations + " Iterations", mesh, gs);

			}

			// print the time it took to run this section
			System.out.println("time took: " + (System.nanoTime() - timeStart) + " ns\n");

			plot.addLinePlot("Initial Guess", mesh, getGuessValue(mesh));

			plot.addLinePlot("True Value", mesh, actualVal);

		}

		{

			final double mu = Math.abs(Math.cos(Math.PI / 257.0)); // largest eigenvalue of point jacobi

			final double omega = 1.8;// 2.0 / (1.0 + Math.sqrt(1.0 - (mu * mu))); // plug in mu to find optimum omega value to use for SOR TODO is this right?

			System.out.println("mesh length: " + mesh.length + ", omega = " + omega);

			final long timeStart = System.nanoTime();

			final CPlot plot = new CPlot("Gauss Seidel with SOR - Phi vs. X");

			for (int iterations = 100; iterations < 501; iterations += 100) {

				// solve for the solutions to the derivatives at each point
				final double[] guess = cInterface.solveSecondDerivativeCentralDifference(
						getGuessValue(mesh), h);

				System.out.println("last vals java - " + guess[guess.length - 1] + ", " + guess[guess.length - 2] + ", " + guess[guess.length - 3]);

				final double[] gs = cInterface.findOneDimensionalGaussSeidelWithSOR(iterations,
						guess, getSecondDerValue(mesh), h, omega, 0.0, 0.0);

				plot.addLinePlot(iterations + " Iterations", mesh, gs);

			}

			// print the time it took to run this section
			System.out.println("time took: " + (System.nanoTime() - timeStart) + " ns\n");

			plot.addLinePlot("Initial Guess", mesh, getGuessValue(mesh));

			plot.addLinePlot("True Value", mesh, actualVal);

		}

	}

	final static double[] getGuessValue(final double[] mesh) {

		final double[] guessVal = new double[mesh.length];

		guessVal[0] = 0.0;

		for (int x = 0; x < guessVal.length; x++) {
			guessVal[x] = ((Math.sin(2.0 * Math.PI * mesh[x])) / 50.0) + ((Math.sin(64.0 * Math.PI * mesh[x])) / 200.0);
		}

		guessVal[guessVal.length - 1] = 0.0;

		return guessVal;

	}

	final static double[] getSecondDerValue(final double[] mesh) {

		final double[] fVal = new double[mesh.length];

		for (int x = 0; x < fVal.length; x++) {

			fVal[x] = (Math.sin(2.0 * Math.PI * mesh[x]) * ((((-5.0 * Math.PI * Math.PI * Math.pow(
					mesh[x], 4.0)) + (4.0 * Math.PI * Math.PI * Math.pow(mesh[x], 3.0)) + (15.0 * Math.pow(
							mesh[x], 2.0))) - (6.0 * mesh[x])))) + (Math.cos(2.0 * Math.PI * mesh[x]) * ((20.0 * Math.PI * Math.pow(
									mesh[x], 3.0)) - (12.0 * Math.PI * Math.pow(mesh[x], 2.0))));

		}

		return fVal;

	}

}