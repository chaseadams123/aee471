package com.school.AEE471;

import javax.swing.JFrame;

import org.math.plot.Plot2DPanel;

public class CPlot {

	private final Plot2DPanel plot;
	private final JFrame frame;

	public CPlot(final String titleName) {

		plot = new Plot2DPanel();

		plot.addLegend("SOUTH");

		frame = new JFrame(titleName);
		plot.setSize(960, 640);
		frame.setSize(960, 640);
		frame.setContentPane(plot);
		frame.setVisible(true);

	}

	public void addLinePlot(final String plotName, double[] xValues, double[] yValues) {
		plot.addLinePlot(plotName, xValues, yValues);
	}

	public void hidePlot() {
		frame.setVisible(false);
	}

}
