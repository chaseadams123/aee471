package com.school.AEE471;

public class CTriDiagonalMatrix {

	private final double[] lower;
	private final double[] middle;
	private final double[] upper;
	private final double[] right;

	public CTriDiagonalMatrix(final double[] f, final double[] h, final int length) {

		final int N = length - 1;

		lower = new double[length];
		middle = new double[length];
		upper = new double[length];
		right = new double[length];

		/**
		 * Initialize the first value of the lower array to a "null" value,
		 * middle to 1, upper to 2
		 */
		lower[0] = Double.MIN_VALUE;
		middle[0] = 1;
		upper[0] = 2;
		right[0] = (((-5 / 2) * f[0]) + (2 * f[1]) + (0.5 * f[2])) / h[0];

		/**
		 * Initialize the median values of the lower array to 1, middle to 4,
		 * upper to 1
		 */
		for (int x = 1; x < (N); x++) {

			lower[x] = 1;
			middle[x] = 4;
			upper[x] = 1;
			right[x] = (3 / h[x]) * (f[x + 1] - f[x - 1]);

		}

		/**
		 * Initialize the last values of the lower array to 2, upper to "null",
		 * and middle to 1
		 */
		lower[N] = 2;
		middle[N] = 1;
		upper[N] = Double.MIN_VALUE;
		right[N] = (((5 / 2) * f[N]) - (2 * f[N - 1]) - (0.5 * f[N - 2])) / h[N];

	}

	public CTriDiagonalMatrix(final double[] f, final double h, final int length) {

		final int N = length - 1;

		lower = new double[length];
		middle = new double[length];
		upper = new double[length];
		right = new double[length];

		/**
		 * Initialize the first value of the lower array to a "null" value,
		 * middle to 1, upper to 2
		 */
		lower[0] = Double.MIN_VALUE;
		middle[0] = 1;
		upper[0] = 2;
		right[0] = (((-5 / 2) * f[0]) + (2 * f[1]) + (0.5 * f[2])) / h;

		/**
		 * Initialize the median values of the lower array to 1, middle to 4,
		 * upper to 1
		 */
		for (int x = 1; x < (N); x++) {

			lower[x] = 1;
			middle[x] = 4;
			upper[x] = 1;
			right[x] = (3 / h) * (f[x + 1] - f[x - 1]);

		}

		/**
		 * Initialize the last values of the lower array to 2, upper to "null",
		 * and middle to 1
		 */
		lower[N] = 2;
		middle[N] = 1;
		upper[N] = Double.MIN_VALUE;
		right[N] = (((5 / 2) * f[N]) - (2 * f[N - 1]) - (0.5 * f[N - 2])) / h;

	}

	public double[] getLower() {
		return lower;
	}

	public double[] getMiddle() {
		return middle;
	}

	public double[] getUpper() {
		return upper;
	}

	public double[] getRight() {
		return right;
	}

}
