package com.school.AEE471;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class Homework3Part2 {

	public static void main(String[] args) throws InterruptedException, FileNotFoundException, UnsupportedEncodingException {

		final CSolver cInterface = new CSolver();

		// define the mesh length and create the mesh array
		final int M = 81;
		final double meshLength = 2.0;
		final double offsetFromZero = -1.0;

		final double[] mesh = new double[M + 1];

		for (double x = 0; x <= M; x++) {
			mesh[(int) x] = (meshLength * (x / M)) + offsetFromZero;
			System.out.println("mesh: " + mesh[(int) x]);
		}

		final double[] guessVal = new double[mesh.length];

		{
			guessVal[0] = 0.0;

			for (int x = 0; x < guessVal.length; x++) {
				guessVal[x] = 0.5 * Math.sin(Math.PI * mesh[x]) * Math.sin(4.0 * Math.PI * mesh[x]);

				System.out.println("guessVal: " + guessVal[x]);
			}

			guessVal[guessVal.length - 1] = 0.0;

		}

		final double[] fVal = new double[mesh.length * mesh.length];

		for (int y = 0; y < mesh.length; y++) {
			for (int x = 0; x < mesh.length; x++) {
				fVal[x] = (10.0 - (10.0 * Math.pow(Math.cos(mesh[x]), 2.0))) + (10.0 * Math.sin(mesh[x]));
			}
		}

		{

			final double mu = 1.0 - (0.25 * ((Math.pow(Math.PI, 2) / Math.pow(M, 2)) + (Math.pow(
					Math.PI, 2) / Math.pow(M, 2)))); // largest eigenvalue of point jacobi

			final double omega = 2.0 / (1.0 + Math.sqrt(1.0 - (mu * mu))); // plug in mu to find optimum omega value to use for SOR

			final double[] meshCurr = mesh;

			System.out.println("mesh length: " + meshCurr.length);

			final long timeStart = System.nanoTime();

			final int size = meshCurr.length;

			// define h based upon the size of the mesh
			final double h = (meshLength / size);

			final double[] fFinal = new double[(M + 1) * (M + 1)];

			// solve for the solutions to the derivatives at each point
			for (int row = 0; row < M; row++) {

				final double[] fAt = cInterface.solveSecondDerivativeCentralDifference(guessVal, h);

				for (int x = 0; x < fAt.length; x++) {
					fFinal[cInterface.get1DIndex(M, row, x)] = fAt[x];
				}

			}

			System.out.println("here");

			final double[][] pj10 = cInterface.convertTo2D(
					cInterface.findTwoDimensionalPointJacobi(600, M, fFinal, fVal, h), M, M);

			final PrintWriter writer = new PrintWriter("output.txt", "UTF-8");

			writer.println("matrix for 10 iters:");
			writer.println("z");

			for (int x = 0; x < M; x++) {

				for (int y = 0; y < M; y++) {

					writer.print(pj10[x][y] + " ");

				}

				writer.println(";");

			}

			writer.close();

			// print the time it took to run this section
			System.out.println("time took: " + (System.nanoTime() - timeStart) + " ns\n");

		}

	}

}