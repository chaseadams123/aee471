package com.school.AEE471;

public class CSolver {

	static {
		System.load("C:\\Users\\Chase\\Dropbox\\School\\AEE 471\\workspace\\AEE471\\c++\\x64\\Debug\\AEE471.dll");
	}

	public CSolver() {

	}

	/*
	 * Homework 2
	 */

	private native double[] findAllNorms(double[] arrayOrig);

	private native double findInfinityNorm(double[] arrayOrig);

	private native double findOneNorm(double[] arrayOrig);

	private native double findTwoNorm(double[] arrayOrig);

	private native double[] findError(double[] ds, double[] arrayOrig);

	private native double[] findErrorOrders(double[] norm1, double[] norm2, double h, double prevH);

	private native double[] triDiagonalSolve(double[] lower, double[] middle, double[] upper,
			double[] right);

	private native double[] centralDifferenceFirstDerivativeSolve(double[] f, double[] hArray,
			boolean firstOrder);

	private native double[] centralDifferenceSecondDerivativeSolve(double[] f, double h,
			boolean firstOrder);

	/*
	 * Homework 3
	 */

	private native double[] find1DPointJacobi(final int iterations, final double[] arrayOrig,
			final double[] f, final double h);

	private native double[] find1DGaussSeidel(int iterations, double[] initialGuess, double[] ds,
			double h, double omega, double zeroBoundaryCondition, double maxBoundaryCondition);

	private native double[] find2DPointJacobi(int iterations, int widthOfPhi,
			double[] initialGuess, double[] secondDerivative, double h);

	private native double[] find2DGaussSeidel(int iterations, int widthOfPhi, double omega,
			double[] initialGuess, double[] secondDerivative, double[] h);

	private native double[] calculateResidual(double[] fGoal, double[] aTimesPhi);

	/**
	 * Gets the 1D index of a two dimensional array.
	 *
	 * @param width
	 * @param row
	 * @param column
	 * @return
	 */
	public int get1DIndex(int width, int row, int column) {
		return ((width * row) + column); // use row major order to find the index mapped to a 1D array - DO NOT MODIFY
	}

	/**
	 * Converts a 1D array to a 2D array
	 *
	 * @param matrix
	 * @param width
	 * @param height
	 * @return
	 */
	public double[][] convertTo2D(double[] matrix, int width, int height) {

		final double[][] returnArray = new double[width][height];

		for (int x = 0; x < width; x++) {

			for (int y = 0; y < height; y++) {

				returnArray[x][y] = matrix[get1DIndex(width, x, y)];

			}

		}

		return returnArray;

	}

	/**
	 * Solves a tridiagonal matrix, defined by its lower, middle, and upper
	 * diagonals, as well as the right hand side.
	 *
	 * @param lower
	 * @param middle
	 * @param upper
	 * @param right
	 * @return solution
	 */
	public double[] solveTriDiagonal(final double[] lower, final double[] middle,
			final double[] upper, final double[] right) {

		if ((((lower.length + middle.length + upper.length + right.length) / 4) - lower.length) != 0) {
			return null;
		}

		return triDiagonalSolve(lower, middle, upper, right);

	}

	/**
	 * Takes a varying size of array (must all be same length) and loops through
	 * to solve the derivative of the values.
	 *
	 * @param fMinusOne
	 * double array
	 * @param fMinusTwo
	 * double array
	 * @param fPlusOne
	 * double array
	 * @param fPlusTwo
	 * double array
	 * @param h
	 * double array (varying)
	 * @return derivative of the given value(s)
	 */
	public double[] solveCentralDifference(final double[] f, final double[] h) {

		if ((((f.length + h.length) / 2) - f.length) != 0) {
			return null;
		}

		return centralDifferenceFirstDerivativeSolve(f, h, true);

	}

	/**
	 * Takes a varying size of array (must all be same length) and loops through
	 * to solve the derivative of the values.
	 *
	 * @param fMinusOne
	 * double array
	 * @param fMinusTwo
	 * double array
	 * @param fPlusOne
	 * double array
	 * @param fPlusTwo
	 * double array
	 * @param h
	 * double array (varying)
	 * @return derivative of the given value(s)
	 */
	public double[] solveCentralDifferenceAccurate(final double[] f, final double[] h) {

		if ((((f.length + h.length) / 2) - f.length) != 0) {
			return null;
		}

		return centralDifferenceFirstDerivativeSolve(f, h, false);

	}

	/**
	 * Takes a varying size of array (must all be same length) and loops through
	 * to solve the derivative of the values.
	 *
	 * @param fMinusOne
	 * double array
	 * @param fMinusTwo
	 * double array
	 * @param fPlusOne
	 * double array
	 * @param fPlusTwo
	 * double array
	 * @param h
	 * (constant)
	 * @return derivative of the given value(s)
	 */
	public double[] solveCentralDifference(final double[] f, final double h) {

		final double[] hArray = new double[f.length];

		for (int x = 0; x < f.length; x++) {
			hArray[x] = h;
		}

		return centralDifferenceFirstDerivativeSolve(f, hArray, true);

	}

	/**
	 * Takes a varying size of array (must all be same length) and loops through
	 * to solve the derivative of the values.
	 *
	 * @param fMinusOne
	 * double array
	 * @param fMinusTwo
	 * double array
	 * @param fPlusOne
	 * double array
	 * @param fPlusTwo
	 * double array
	 * @param h
	 * (constant)
	 * @return derivative of the given value(s)
	 */
	public double[] solveCentralDifferenceAccurate(final double[] f, final double h) {

		final double[] hArray = new double[f.length];

		for (int x = 0; x < f.length; x++) {
			hArray[x] = h;
		}

		return centralDifferenceFirstDerivativeSolve(f, hArray, false);

	}

	/**
	 * Takes a varying size of array (must all be same length) and loops through
	 * to solve the second derivative of the values.
	 *
	 * @param fMinusOne
	 * double array
	 * @param fMinusTwo
	 * double array
	 * @param fPlusOne
	 * double array
	 * @param fPlusTwo
	 * double array
	 * @param h
	 * (constant)
	 * @return second derivative of the given value(s)
	 */
	public double[] solveSecondDerivativeCentralDifference(final double[] f, final double h) {

		return centralDifferenceSecondDerivativeSolve(f, h, true);

	}

	/**
	 * Solves for the infinity norm given an array.
	 *
	 * @param array
	 * @return infinity norm
	 */
	public double findInfNorm(final double[] array) {
		return findInfinityNorm(array);
	}

	/**
	 * Solves for the one norm given an array.
	 *
	 * @param array
	 * @return one norm
	 */
	public double find1Norm(final double[] array) {
		return findOneNorm(array);
	}

	/**
	 * Solves for the two norm given an array.
	 *
	 * @param array
	 * @return two norm
	 */
	public double find2Norm(final double[] array) {
		return findTwoNorm(array);
	}

	/**
	 * Solves for the infinity norm, one norm, and two norm given an array.
	 *
	 * @param array
	 * @return array of infinity norm at [0], one norm at [1], two norm at [2]
	 */
	public double[] findAllNorm(final double[] array) {
		return findAllNorms(array);
	}

	/**
	 * Solves for the error between the actual value and an array of values.
	 *
	 * @param ds
	 * values to solve the error for
	 * @param array
	 * original array
	 * @return two norm
	 */
	public double[] findErrorValue(final double[] ds, final double[] array) {
		return findError(ds, array);
	}

	/**
	 * Solves for order of the provided error norms
	 *
	 * @param array
	 * original array
	 * @return order of norms
	 */
	public double[] findAllErrorOrders(final double[] norm1, final double[] norm2, final double h,
			final double prevH) {
		return findErrorOrders(norm1, norm2, h, prevH);
	}

	/**
	 * Finds the one-dimensional Point Jacobi
	 *
	 * @param iterations
	 * @param initialGuess
	 * @param secondDerivative
	 * @param h
	 * @return
	 */
	@Deprecated
	public double[] findOneDimensionalPointJacobi(final int iterations,
			final double[] initialGuess, final double[] secondDerivative, final double h) {

		return find1DPointJacobi(iterations, initialGuess, secondDerivative, h);

	}

	/**
	 * Finds the one-dimensional Gauss Seidel with successive over relaxation.
	 *
	 * @param iterations
	 * @param initialGuess
	 * @param secondDerivative
	 * @param h
	 * @param omega
	 * @param zeroBoundaryCondition
	 * @param maxBoundaryCondition
	 * @return
	 */
	public double[] findOneDimensionalGaussSeidelWithSOR(int iterations, double[] initialGuess,
			double[] secondDerivative, double h, double omega, double zeroBoundaryCondition,
			double maxBoundaryCondition) {

		return find1DGaussSeidel(iterations, initialGuess, secondDerivative, h, omega,
				zeroBoundaryCondition, maxBoundaryCondition);

	}

	/**
	 * Finds the Gauss Seidel with the SOR = 1.
	 *
	 * @param iterations
	 * @param f
	 * @param ds
	 * @param h
	 * @param zeroBoundaryCondition
	 * @param maxBoundaryCondition
	 * @return
	 */
	public double[] findOneDimensionalGaussSeidel(int iterations, double[] f, double[] ds,
			double h, double zeroBoundaryCondition, double maxBoundaryCondition) {

		return find1DGaussSeidel(iterations, f, ds, h, 1.0, zeroBoundaryCondition,
				maxBoundaryCondition);

	}

	/**
	 * Finds the two-dimensional solution using the Point-Jacobi method
	 *
	 * @param iterations
	 * @param widthOfPhi
	 * @param initialGuess
	 * @param secondDerivative
	 * @param h
	 * @return
	 */
	@Deprecated
	public double[] findTwoDimensionalPointJacobi(int iterations, int widthOfPhi,
			double[] initialGuess, double[] secondDerivative, double h) {

		return find2DPointJacobi(iterations, widthOfPhi, initialGuess, secondDerivative, h);

	}

	/**
	 * Finds the two-dimensional solution using the Gauss Seidel method with SOR
	 *
	 * @param iterations
	 * @param widthOfPhi
	 * @param initialGuess
	 * @param secondDerivative
	 * @param h
	 * @param omega
	 * @return
	 */
	public double[] findTwoDimensionalGaussSeidelWithSOR(int iterations, int widthOfPhi,
			double[] initialGuess, double[] secondDerivative, double h, double omega) {

		final double[] hArray = new double[initialGuess.length];

		for (int x = 0; x < initialGuess.length; x++) {
			hArray[x] = h;
		}

		return find2DGaussSeidel(iterations, widthOfPhi, omega, initialGuess, secondDerivative,
				hArray);

	}

	/**
	 * Finds the two-dimensional solution using the Gauss Seidel method without
	 * SOR
	 *
	 * @param iterations
	 * @param widthOfPhi
	 * @param initialGuess
	 * @param secondDerivative
	 * @param h
	 * @return
	 */
	public double[] findTwoDimensionalGaussSeidel(int iterations, int widthOfPhi,
			double[] initialGuess, double[] secondDerivative, double h) {

		final double[] hArray = new double[initialGuess.length];

		for (int x = 0; x < initialGuess.length; x++) {
			hArray[x] = h;
		}

		return find2DGaussSeidel(iterations, widthOfPhi, 1.0, initialGuess, secondDerivative,
				hArray);

	}

}