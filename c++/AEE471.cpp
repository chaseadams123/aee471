#include <jni.h>
#include <stdio.h>
#include <cmath>
#include <iostream>
#include <algorithm>
#include "AEE471.h"

JNIEXPORT jdoubleArray JNICALL Java_com_school_AEE471_CSolver_triDiagonalSolve(JNIEnv *env, jobject obj, jdoubleArray lower, jdoubleArray middle, jdoubleArray upper, jdoubleArray right) {

	printf("starting tridiagonal solver\n");

	/**
	 * CREATE VARIABLES
	 **/

	jdoubleArray result; // create the result array
	const jsize length = env->GetArrayLength(lower); // get the length of the arrays
	const int N = length - 1; // instantiate a variable to hold the N value

	result = env->NewDoubleArray(length); // instantiate the reuslt array

	if (result == NULL) {
		return NULL; // out of memory error
	}

	// Get the pointers to the primitive arrays - do not call another JNI function between this and release
	jdouble *cLower = (jdouble*)env->GetPrimitiveArrayCritical(lower, 0);
	jdouble *cMiddle = (jdouble*) env->GetPrimitiveArrayCritical(middle, 0);
	jdouble *cUpper = (jdouble*) env->GetPrimitiveArrayCritical(upper, 0);
	jdouble *cRight = (jdouble*) env->GetPrimitiveArrayCritical(right, 0);

	/**
	 * ELIMINATION
	 **/

	for (int i = 1; i < N; i++) { // loop through to eliminate the lower row, a_(i) - from 1->N

		cMiddle[i] = cMiddle[i] - ((cUpper[i - 1]) * (cLower[i]) / (cMiddle[i - 1])); // b_(i) = b_(i) - (c_(i-1)*(a_(i)/b_(i-1))
		cRight[i] = cRight[i] - (cRight[i - 1] * (cLower[i]) / (cMiddle[i - 1])); // d_(i) = d_(i) - (d_(i-1)*a_(i)/b_(i-1))

	}

	env->ReleasePrimitiveArrayCritical(lower, cLower, 0); // release cLower since it is not used anymore (c_Lower = 0)

	/**
 	 * BACK SUBSTITUTION
	 **/

	cRight[N] = (cRight[N]) / (cMiddle[N]); // d_(N) = d_(N)/b_(N)

	for (int i = (N - 1); i >= 0; i--) { // loop through to back substitute and eliminate the upper row, c_(i) - from N-1->0

		cRight[i] = (cRight[i] - ((cUpper[i] * cRight[i + 1]))) / cMiddle[i]; // d_(i) = (d_(i) - c_(i)*d_(i+1))/b_(i)

	}

	/**
	 * RELEASE VALUES
	 **/

	env->SetDoubleArrayRegion(result, 0, length, cRight); // move from the temp structure to the java structure

	env->ReleasePrimitiveArrayCritical(right, cRight, 0); // release cRight since it is not used anymore
	env->ReleasePrimitiveArrayCritical(upper, cUpper, 0); // release cRight since it is not used anymore
	env->ReleasePrimitiveArrayCritical(middle, cMiddle, 0); // release cMiddle since it is not used anymore

	fflush(stdout);

	return result;

}

JNIEXPORT jdoubleArray JNICALL Java_com_school_AEE471_CSolver_centralDifferenceFirstDerivativeSolve(JNIEnv *env, jobject obj, jdoubleArray fArray, jdoubleArray h, jboolean firstOrder) {

	printf("starting central difference solver\n");

	/**
	 * CREATE VARIABLES
	 **/

	jdoubleArray result; // create the result array
	const jsize length = env->GetArrayLength(h);

	result = env->NewDoubleArray(length);

	if (result == NULL) {
		return NULL; // out of memory error
	}
	
	// Get the pointers to the primitive arrays - do not call another JNI function between this and release
	jdouble *cArray = (jdouble*) env->GetPrimitiveArrayCritical(fArray, 0);
	jdouble *cH = (jdouble*)env->GetPrimitiveArrayCritical(h, 0);

	jdouble* tempResult = env->GetDoubleArrayElements(result, 0);

	const int N = length - 1; // instantiate a variable to hold the N value

	/**
	 * FORWARD DIFFERENCE FOR INITIAL POINT
	 **/

	if (firstOrder) {
		tempResult[0] = (cArray[1] - cArray[0]) / cH[0]; // O(1)
	}
	else {
		tempResult[0] = (-cArray[2] + (4 * cArray[1]) - (3 * cArray[0])) / (2 * cH[0]); // O(2)
	}

	/**
	 * CENTRAL DIFFERENCE - O(2)
	 **/

	for (int i = 1; i < N; i++) { // loop through all of the given central difference vectors
		
		tempResult[i] = (cArray[i + 1] - cArray[i - 1]) / (2 * cH[i]); // plug the values into the central diff formula

	}

	/**
	* BACKWARD DIFFERENCE FOR FINAL POINT
	**/

	if (firstOrder) {
		tempResult[N] = (cArray[N] - cArray[N - 1]) / cH[N]; // O(1)
	}
	else {
		tempResult[N] = ((3 * cArray[N]) - (4 * cArray[N - 1]) + (cArray[N - 2])) / (2 * cH[N]); // O(2)
	}

	/**
	 * RELEASE VALUES
	 **/

	env->SetDoubleArrayRegion(result, 0, length, tempResult); // move from the temp structure to the java structure

	env->ReleasePrimitiveArrayCritical(fArray, cArray, 0); // release since it is not used anymore
	env->ReleasePrimitiveArrayCritical(h, cH, 0); // release since it is not used anymore

	fflush(stdout);

	return result;

}

JNIEXPORT jdoubleArray JNICALL Java_com_school_AEE471_CSolver_findError(JNIEnv *env, jobject obj, jdoubleArray actualValues, jdoubleArray arrayOrig) {

	printf("starting error finder\n");

	/**
	 * CREATE VARIABLES
	 **/

	// Get the pointer to the primitive array - do not call another JNI function between this and release
	jdouble *cArray = (jdouble*)env->GetPrimitiveArrayCritical(arrayOrig, 0);
	jdouble *cActualValues = (jdouble*)env->GetPrimitiveArrayCritical(actualValues, 0);

	jdoubleArray result; // create the result array
	const jsize length = env->GetArrayLength(arrayOrig);

	result = env->NewDoubleArray(length);

	if (result == NULL) {
		return NULL; // out of memory error
	}

	jdouble* tempResult = (jdouble*) env->GetPrimitiveArrayCritical(result, 0);

	/**
	 * FIND ERROR
	 **/

	for (int i = 0; i < length; i++) { // loop through all of the given values to find the error

		tempResult[i] = cArray[i] - cActualValues[i];

	}

	/**
	 * RELEASE VALUES
	 **/

	env->ReleasePrimitiveArrayCritical(arrayOrig, cArray, 0); // release since it is not used anymore
	env->ReleasePrimitiveArrayCritical(actualValues, cActualValues, 0); // release since it is not used anymore
	env->SetDoubleArrayRegion(result, 0, length, tempResult); // move from the temp structure to the java structure

	fflush(stdout);

	return result;

}

JNIEXPORT jdouble JNICALL Java_com_school_AEE471_CSolver_findInfinityNorm(JNIEnv *env, jobject obj, jdoubleArray arrayOrig) {

	printf("starting max value finder\n");

	/**
	 * CREATE VARIABLES
	 **/

	// Get the pointer to the primitive array - do not call another JNI function between this and release
	jdouble *cArray = (jdouble*) env->GetPrimitiveArrayCritical(arrayOrig, 0);

	jdouble result = cArray[0]; // set the max result equal to the initial value
	const jsize length = env->GetArrayLength(arrayOrig);

	if (result == NULL) {
		return NULL; // out of memory error
	}

	/**
	 * FIND MAXIMUM
	 **/

	jdouble temp;

	for (int i = 1; i < length; i++) { // loop through all of the given values to find the maximum

		temp = abs(cArray[i]);

		if (temp > result) {
			result = temp;
		}

	}

	/**
	 * RELEASE VALUES
	 **/

	env->ReleasePrimitiveArrayCritical(arrayOrig, cArray, 0); // release since it is not used anymore

	fflush(stdout);

	return result;

}

JNIEXPORT jdouble JNICALL Java_com_school_AEE471_CSolver_findOneNorm(JNIEnv *env, jobject obj, jdoubleArray arrayOrig) {

	printf("starting one norm finder\n");

	/**
	 * CREATE VARIABLES
	 **/

	// Get the pointer to the primitive array - do not call another JNI function between this and release
	jdouble *cArray = (jdouble*) env->GetPrimitiveArrayCritical(arrayOrig, 0);

	jdouble result; // initialize the sum
	result = 0.0; // set the initial result to zero

	const jdouble length = env->GetArrayLength(arrayOrig);

	/**
	 * FIND ONE-NORM
	 **/

	for (int i = 0; i < length; i++) { // loop through all of the given values to find the sum
		result += abs(cArray[i]);
	}

	result *= 1.0 / (length); // multiply by 1/(N+1)

	/**
	 * RELEASE VALUES
	 **/

	env->ReleasePrimitiveArrayCritical(arrayOrig, cArray, 0); // release since it is not used anymore

	fflush(stdout);

	return result;

}

JNIEXPORT jdouble JNICALL Java_com_school_AEE471_CSolver_findTwoNorm(JNIEnv *env, jobject obj, jdoubleArray arrayOrig) {

	printf("starting two norm finder\n");

	/**
	 * CREATE VARIABLES
	 **/

	// Get the pointer to the primitive array - do not call another JNI function between this and release
	jdouble *cArray = (jdouble*) env->GetPrimitiveArrayCritical(arrayOrig, 0);

	jdouble result; // initialize the result variable
	result = 0.0; // set the initial sum to zero

	const jsize length = env->GetArrayLength(arrayOrig);

	/**
	 * FIND TWO-NORM
	 **/

	for (int i = 0; i < length; i++) { // loop through all of the given values to find the sum of the squares
		result += pow(cArray[i], 2.0);
	}

	result *= 1.0 / (length); // multiply by 1/(N+1)

	result = sqrt(result); // get the square root of the value

	/**
	 * RELEASE VALUES
	 **/

	env->ReleasePrimitiveArrayCritical(arrayOrig, cArray, 0); // release since it is not used anymore

	fflush(stdout);

	return result;

}

JNIEXPORT jdoubleArray JNICALL Java_com_school_AEE471_CSolver_findAllNorms(JNIEnv *env, jobject obj, jdoubleArray arrayOrig) {

	printf("starting all norms finder\n");

	/**
	* CREATE VARIABLES
	**/

	jdoubleArray result; // create the result array

	result = env->NewDoubleArray(3); // create an array of length 3 to hold the norms

	if (result == NULL) {
		return NULL; // out of memory error
	}

	jdouble* tempResult = (jdouble*)env->GetPrimitiveArrayCritical(result, 0);

	/**
	* FIND NORMS
	**/

	tempResult[0] = Java_com_school_AEE471_CSolver_findInfinityNorm(env, obj, arrayOrig);
	tempResult[1] = Java_com_school_AEE471_CSolver_findOneNorm(env, obj, arrayOrig);
	tempResult[2] = Java_com_school_AEE471_CSolver_findTwoNorm(env, obj, arrayOrig);

	/**
	* RELEASE VALUES
	**/

	env->ReleasePrimitiveArrayCritical(result, tempResult, 0); // release since it is not used anymore

	fflush(stdout);

	return result;

}

JNIEXPORT jdoubleArray JNICALL Java_com_school_AEE471_CSolver_findErrorOrders(JNIEnv *env, jobject obj, jdoubleArray norm1, jdoubleArray norm2, jdouble h1, jdouble h2) {

	printf("starting norm orders finder\n");

	/**
	* CREATE VARIABLES
	**/

	// Get the pointer to the primitive array - do not call another JNI function between this and release
	jdouble *cNormOne = (jdouble*)env->GetPrimitiveArrayCritical(norm1, 0);
	jdouble *cNormTwo = (jdouble*)env->GetPrimitiveArrayCritical(norm2, 0);

	jdoubleArray result; // create the result array
	const jsize length = env->GetArrayLength(norm1);

	result = env->NewDoubleArray(length);

	if (result == NULL) {
		return NULL; // out of memory error
	}

	jdouble* tempResult = (jdouble*)env->GetPrimitiveArrayCritical(result, 0);

	/**
	* FIND ORDERS OF NORMS
	**/

	for (int i = 0; i < length; i++) { // loop through all of the given values to find the order of the norms given
		tempResult[i] = (log(cNormOne[i] / cNormTwo[i])) / (log(h1 / h2));
	}

	/**
	* RELEASE VALUES
	**/

	env->ReleasePrimitiveArrayCritical(norm1, cNormOne, 0); // release since it is not used anymore
	env->ReleasePrimitiveArrayCritical(norm2, cNormTwo, 0); // release since it is not used anymore

	env->SetDoubleArrayRegion(result, 0, length, tempResult); // move from the temp structure to the java structure

	fflush(stdout);

	return result;

}

JNIEXPORT jdoubleArray JNICALL Java_com_school_AEE471_CSolver_calculateResidual(JNIEnv *env, jobject obj, jdoubleArray fGoal, jdoubleArray aTimesPhi) {

	printf("starting residual finder\n");

	/**
	* CREATE VARIABLES
	**/

	// Get the pointer to the primitive array - do not call another JNI function between this and release
	jdouble *cGoal = (jdouble*)env->GetPrimitiveArrayCritical(fGoal, 0);
	jdouble *cGuess = (jdouble*)env->GetPrimitiveArrayCritical(aTimesPhi, 0);

	jdoubleArray result; // create the result array
	const jsize length = env->GetArrayLength(fGoal);

	result = env->NewDoubleArray(length);

	if (result == NULL) {
		return NULL; // out of memory error
	}

	jdouble* tempResidual = (jdouble*)env->GetPrimitiveArrayCritical(result, 0);

	for (int i = 0; i < length; i++) { // loop through 0->N of the given values to find the residual

		tempResidual[i] = cGoal[i] - cGuess[i];

	}

	/**
	* RELEASE VALUES
	**/

	env->ReleasePrimitiveArrayCritical(aTimesPhi, cGuess, 0); // release since it is not used anymore
	env->ReleasePrimitiveArrayCritical(fGoal, cGoal, 0); // release since it is not used anymore
	env->SetDoubleArrayRegion(result, 0, length, tempResidual); // move from the temp structure to the java structure

	fflush(stdout);

	return result;

}

JNIEXPORT jdoubleArray JNICALL Java_com_school_AEE471_CSolver_centralDifferenceSecondDerivativeSolve(JNIEnv *env, jobject obj, jdoubleArray fArray, jdouble h, jboolean firstOrder) {

	printf("starting second derivative central difference solver\n");

	/**
	* CREATE VARIABLES
	**/

	jdoubleArray result; // create the result array
	const jsize length = env->GetArrayLength(fArray);

	result = env->NewDoubleArray(length);

	if (result == NULL) {
		return NULL; // out of memory error
	}

	// Get the pointers to the primitive arrays - do not call another JNI function between this and release
	jdouble *cArray = (jdouble*)env->GetPrimitiveArrayCritical(fArray, 0);

	jdouble* tempResult = env->GetDoubleArrayElements(result, 0);

	const int N = length - 1; // instantiate a variable to hold the N value

	/**
	* FORWARD DIFFERENCE FOR INITIAL POINT
	**/

	if (firstOrder) {
		tempResult[0] = (cArray[2] - 2.0 * cArray[1] + cArray[0]) / (pow(h, 2.0)); // O(1)
	}
	else {
		tempResult[0] = (-cArray[3] + 4.0 * cArray[2] - 5.0 * cArray[1] + 2.0 * cArray[0]) / (pow(h, 2.0)); // O(2)
	}

	/**
	* CENTRAL DIFFERENCE - O(2)
	**/

	for (int i = 1; i < N; i++) { // loop through all of the given central difference vectors

		tempResult[i] = (cArray[i + 1] - 2.0 * cArray[i] + cArray[i - 1]) / (pow(h, 2.0)); // plug the values into the central diff formula for the second derivative

	}

	/**
	* BACKWARD DIFFERENCE FOR FINAL POINT
	**/

	if (firstOrder) {
		tempResult[N] = (cArray[N] - 2.0 * cArray[N - 1] + cArray[N - 2]) / (pow(h, 2.0)); // O(1)
	}
	else {
		tempResult[N] = (2.0 * cArray[N] - 5.0 * cArray[N - 1] + 4.0 * cArray[N - 2] - cArray[N - 3]) / (pow(h, 2.0)); // O(2)
	}

	/**
	* RELEASE VALUES
	**/

	env->SetDoubleArrayRegion(result, 0, length, tempResult); // move from the temp structure to the java structure

	env->ReleasePrimitiveArrayCritical(fArray, cArray, 0); // release since it is not used anymore

	fflush(stdout);

	return result;

}

JNIEXPORT jdoubleArray JNICALL Java_com_school_AEE471_CSolver_find1DPointJacobi(JNIEnv *env, jobject obj, jint iterations, jdoubleArray phiOrig, jdoubleArray f, jdouble h) {

	printf("starting 1D point jacobi finder\n");

	/**
	* CREATE VARIABLES
	**/

	// Get the pointer to the primitive array - do not call another JNI function between this and release
	jdouble *cPhiOrig = (jdouble*)env->GetPrimitiveArrayCritical(phiOrig, 0);
	jdouble *cF = (jdouble*)env->GetPrimitiveArrayCritical(f, 0);

	jdoubleArray result; // create the result array
	const jsize length = env->GetArrayLength(phiOrig);

	int k = 0; // create the counting variable for the iterations
	bool converged = false; // create the variable used to define convergence

	result = env->NewDoubleArray(length);

	if (result == NULL) {
		return NULL; // out of memory error
	}

	jdouble* cPhiNew = (jdouble*)env->GetPrimitiveArrayCritical(result, 0);

	/**
	* FIND PHI(i) USING THE POINT-JACOBI METHOD
	**/

	while ((!converged && (iterations == -1)) || ((k < iterations) && (iterations != -1))) {

		for (int i = 1; i < (length - 1); i++) { // loop through 1->N-1 and 1->M-1 of the given values to find PHI(i)

			cPhiNew[i] = (0.5 * (cPhiOrig[i + 1] + cPhiOrig[i - 1])) - (0.5 * (pow(h, 2)) * cF[i]);

		}

		// copy the new array into the old array space (slow, but necessary for JNI)
		for (int x = 0; x < length; x++) {
			cPhiOrig[x] = cPhiNew[x];
		}

		if ((k == 0) || (k == 1)) {
			printf("values at = %f, %f, %f \n", cPhiOrig[length - 1], cPhiOrig[length - 2], cPhiOrig[length - 3]);
		}

		k++;

	}

	printf("iters = %d\n", k);

	/**
	* RELEASE VALUES
	**/

	env->ReleasePrimitiveArrayCritical(phiOrig, cPhiOrig, 0); // release since it is not used anymore
	env->ReleasePrimitiveArrayCritical(f, cF, 0); // release since it is not used anymore

	fflush(stdout);

	return result;

}

JNIEXPORT jdoubleArray JNICALL Java_com_school_AEE471_CSolver_find1DGaussSeidel(JNIEnv *env, jobject obj, jint iterations, jdoubleArray phiOrig, jdoubleArray f, jdouble h, jdouble omega, jdouble zeroBC, jdouble maxBC) {

	printf("starting 1D gauss seidel finder\n");

	/**
	* CREATE VARIABLES
	**/

	// Get the pointer to the primitive array - do not call another JNI function between this and release
	jdouble *cPhiOrig = (jdouble*)env->GetPrimitiveArrayCritical(phiOrig, 0);
	jdouble *cF = (jdouble*)env->GetPrimitiveArrayCritical(f, 0);

	const jsize length = env->GetArrayLength(phiOrig);

	int k = 0; // create the counting variable for the iterations
	bool converged = false; // create the variable used to define convergence

	/**
	* FIND PHI(i) USING THE GAUSS-SEIDEL METHOD
	**/

	cPhiOrig[0] = zeroBC; // set the leftmost boundary condition
	cPhiOrig[length - 1] = maxBC; // set the rightmost boundary condition

	printf("values - %f, %f, %f\n", cPhiOrig[length - 1], cPhiOrig[length - 2], cPhiOrig[length - 3]);

	while ((!converged && (iterations == -1)) || ((k < iterations) && (iterations != -1))) {

		for (int i = 1; i < (length - 1); i++) { // loop through 1->N-1 and 1->M-1 of the given values to find PHI(i)

			cPhiOrig[i] = cPhiOrig[i] + omega * ((0.5 * (cPhiOrig[i + 1] + cPhiOrig[i - 1])) - (0.5 * (pow(h, 2.0)) * cF[i]) - cPhiOrig[i]);

		}

		if ((k == 0) || (k == 1)) {
			printf("values at = %f, %f, %f \n", cPhiOrig[length - 1], cPhiOrig[length - 2], cPhiOrig[length - 3]);
		}

		k++;

	}

	printf("iters = %d\n", k);

	/**
	* RELEASE VALUES
	**/

	env->ReleasePrimitiveArrayCritical(phiOrig, cPhiOrig, 0); // release since it is not used anymore
	env->ReleasePrimitiveArrayCritical(f, cF, 0); // release since it is not used anymore

	fflush(stdout);

	return phiOrig;

}

int get1DIndex(int width, int row, int column) {
	return (width * row + column); // use row major order to find the index mapped to a 1D array - DO NOT MODIFY
}

JNIEXPORT jdoubleArray JNICALL Java_com_school_AEE471_CSolver_find2DPointJacobi(JNIEnv *env, jobject obj, jint iterations, jint width, jdoubleArray phiOrig, jdoubleArray f, jdouble h) {

	printf("starting 2D point jacobi finder\n");

	/**
	* CREATE VARIABLES
	**/

	// Get the pointer to the primitive array - do not call another JNI function between this and release
	jdouble *cPhiOrig = (jdouble*)env->GetPrimitiveArrayCritical(phiOrig, 0);
	jdouble *cF = (jdouble*)env->GetPrimitiveArrayCritical(f, 0);

	jdoubleArray result; // create the result array
	const jsize length = env->GetArrayLength(phiOrig);

	int k = 0; // create the counting variable for the iterations
	bool converged = false; // create the variable used to define convergence

	result = env->NewDoubleArray(length);

	if (result == NULL) {
		return NULL; // out of memory error
	}

	jdouble* cPhiNew = (jdouble*)env->GetPrimitiveArrayCritical(result, 0);

	/**
	* FIND PHI(i) USING THE POINT-JACOBI METHOD
	**/

	while ((!converged && (iterations == -1)) || ((k < iterations) && (iterations != -1))) {

		for (int i = 1; i < (width - 2); i++) { // loop through 1->N-1 and 1->M-1 of the given values to find PHI(i,j)

			for (int j = 1; j < (width - 2); j++) {

				cPhiNew[get1DIndex(width, i, j)] = (0.25*(cPhiOrig[get1DIndex(width, i - 1, j)] + cPhiOrig[get1DIndex(width, i + 1, j)] +
					cPhiOrig[get1DIndex(width, i, j - 1)] + cPhiOrig[get1DIndex(width, i, j + 1)]) -
					(0.25 * pow(h, 2.0) * cF[get1DIndex(width, i, j)]));

			}

		}

		// copy the new array into the old array space (slow, but necessary for JNI)
		for (int x = 0; x < width; x++) {
			cPhiOrig[x] = cPhiNew[x];
		}

		if ((k == 0) || (k == 1)) {
			printf("values at = %f, %f, %f \n", cPhiOrig[length - 1], cPhiOrig[length - 2], cPhiOrig[length - 3]);
		}

		k++;

	}

	printf("iters = %d\n", k);

	/**
	* RELEASE VALUES
	**/

	env->ReleasePrimitiveArrayCritical(phiOrig, cPhiOrig, 0); // release since it is not used anymore
	env->ReleasePrimitiveArrayCritical(f, cF, 0); // release since it is not used anymore

	fflush(stdout);

	return result;

}

JNIEXPORT jdoubleArray JNICALL Java_com_school_AEE471_CSolver_find2DGaussSeidel(JNIEnv *env, jobject obj, jint iterations, jint width, jdouble omega, jdoubleArray phiOrig, jdoubleArray f, jdouble h, jdouble zeroBC, jdouble maxBC) {

	printf("starting 2D gauss seidel finder\n");

	/**
	* CREATE VARIABLES
	**/

	// Get the pointer to the primitive array - do not call another JNI function between this and release
	jdouble *cPhiOrig = (jdouble*)env->GetPrimitiveArrayCritical(phiOrig, 0);
	jdouble *cF = (jdouble*)env->GetPrimitiveArrayCritical(f, 0);

	const jsize length = env->GetArrayLength(phiOrig);

	int k = 0; // create the counting variable for the iterations
	bool converged = false; // create the variable used to define convergence

	/**
	* FIND PHI(i) USING THE GAUSS-SEIDEL METHOD
	**/

	cPhiOrig[0] = zeroBC; // set the leftmost boundary condition
	cPhiOrig[length - 1] = maxBC; // set the rightmost boundary condition

	while ((!converged && (iterations == -1)) || ((k < iterations) && (iterations != -1))) {

		for (int i = 1; i < (width - 2); i++) { // loop through 1->N-1 and 1->M-1 of the given values to find PHI(i,j)

			for (int j = 1; j < (width - 2); j++) {

				cPhiOrig[get1DIndex(width, i, j)] = cPhiOrig[get1DIndex(width, i, j)] +
					omega*(0.25*(cPhiOrig[get1DIndex(width, i, j - 1)] + cPhiOrig[get1DIndex(width, i - 1, j)] +
					cPhiOrig[get1DIndex(width, i + 1, j)] + cPhiOrig[get1DIndex(width, i, j + 1)]) -
					(0.25 * pow(h, 2) * cF[get1DIndex(width, i, j)]) -
					cPhiOrig[get1DIndex(width, i, j)]);

			}

		}

		k++;

	}

	printf("iters = %d\n", k);

	/**
	* RELEASE VALUES
	**/

	env->ReleasePrimitiveArrayCritical(phiOrig, cPhiOrig, 0); // release since it is not used anymore
	env->ReleasePrimitiveArrayCritical(f, cF, 0); // release since it is not used anymore

	fflush(stdout);

	return phiOrig;

}
